import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int firstNum, secondNum;
        System.out.print("Please input first number: ");
        firstNum = sc.nextInt();
        System.out.print("Please input second number: ");
        secondNum = sc.nextInt();
        System.out.println(firstNum+" "+secondNum);
        if(firstNum<secondNum){
            for(int i=firstNum; i<=secondNum;i++){
               System.out.print(i+" "); 
            }
        }else{
            System.out.println("Error");
        }
        sc.close();
    }
}
